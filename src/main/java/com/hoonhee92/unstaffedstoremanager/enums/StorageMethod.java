package com.hoonhee92.unstaffedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StorageMethod {
    RT("실온"),
    COLD("냉장"),
    FROZEN("냉동");

    private final String storageName;
}
