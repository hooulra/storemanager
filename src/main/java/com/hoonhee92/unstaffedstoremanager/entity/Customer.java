package com.hoonhee92.unstaffedstoremanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Customer {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 연락처
    @Column(nullable = false, unique = true)
    private String contact;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 적립금
    @Column(nullable = false)
    private Double point;
}
