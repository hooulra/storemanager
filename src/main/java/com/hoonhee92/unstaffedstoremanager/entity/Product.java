package com.hoonhee92.unstaffedstoremanager.entity;

import com.hoonhee92.unstaffedstoremanager.enums.StorageMethod;
import com.hoonhee92.unstaffedstoremanager.interfaces.CommonModelBuilder;
import com.hoonhee92.unstaffedstoremanager.model.ProductRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품명
    @Column(nullable = false)
    private String productName;

    // 단가
    @Column(nullable = false)
    private Double unitPrice;

    // 할인금액
    @Column(nullable = false)
    private Double discountPrice;

    // 바코드
    @Column(nullable = false)
    private String barcode;

    // 재고수량
    @Column(nullable = false)
    private Integer quantity;

    // 보관방법
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StorageMethod storageMethod;


    private Product(Builder builder) {
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.discountPrice = builder.discountPrice;
        this.barcode = builder.barcode;
        this.quantity = builder.quantity;
        this.storageMethod = builder.storageMethod;
    }


    public static class Builder implements CommonModelBuilder<Product> {
        private final String productName;
        private final Double unitPrice;
        private final Double discountPrice;
        private final String barcode;
        private final Integer quantity;
        private final StorageMethod storageMethod;

        public Builder(ProductRequest request) {
            this.productName = request.getProductName();
            this.unitPrice = request.getUnitPrice();
            this.discountPrice = request.getDiscountPrice();
            this.barcode = request.getBarcode();
            this.quantity = request.getQuantity();
            this.storageMethod = request.getStorageMethod();
        }
        @Override
        public Product build() {
            return new Product(this);
        }
    }

}
