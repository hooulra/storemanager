package com.hoonhee92.unstaffedstoremanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Calculation {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품명
    @Column(nullable = false)
    private String productName;

    // 단가
    @Column(nullable = false)
    private Double unitPrice;

    // 수량
    @Column(nullable = false)
    private Integer quantity;

    // 금액
    @Column(nullable = false)
    private Double price;


}
