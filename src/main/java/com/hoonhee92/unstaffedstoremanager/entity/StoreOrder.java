package com.hoonhee92.unstaffedstoremanager.entity;

import com.hoonhee92.unstaffedstoremanager.interfaces.CommonModelBuilder;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderRequest;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderUpdateInfoRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreOrder {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품 id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    // 원가
    @Column(nullable = false)
    private Double costPrice;

    // 발주 수량
    @Column(nullable = false)
    private Integer orderQuantity;

    // 발주일
    @Column(nullable = false)
    private LocalDate dateOrder;

    // 수정일
    private LocalDate dateRevision;


    public void putStoreOrder(StoreOrderUpdateInfoRequest request) {
        this.costPrice = request.getCostPrice();
        this.orderQuantity = request.getOrderQuantity();
        this.dateRevision = LocalDate.now();
    }

    public StoreOrder(Builder builder) {
        this.product = builder.product;
        this.costPrice = builder.costPrice;
        this.orderQuantity = builder.orderQuantity;
        this.dateOrder = LocalDate.now();
        this.dateRevision = LocalDate.EPOCH;

    }

    public static class Builder implements CommonModelBuilder<StoreOrder> {
        private final Product product;
        private final Double costPrice;
        private final Integer orderQuantity;

        public Builder(Product product, StoreOrderRequest request) {
            this.product = product;
            this.costPrice = request.getCostPrice();
            this.orderQuantity = request.getOrderQuantity();
        }
        @Override
        public StoreOrder build() {
            return new StoreOrder(this);
        }
    }

}
