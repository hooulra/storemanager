package com.hoonhee92.unstaffedstoremanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class StoreOut {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품명
    @Column(nullable = false)
    private String productName;

    // 단가
    @Column(nullable = false)
    private Double unitPrice;

    // 결제일
    @Column(nullable = false)
    private LocalDate datePayment;

    // 금액
    @Column(nullable = false)
    private Double price;
}
