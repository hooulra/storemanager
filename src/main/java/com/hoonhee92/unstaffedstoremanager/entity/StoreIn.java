package com.hoonhee92.unstaffedstoremanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class StoreIn {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 발주 id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeOrderId")
    private StoreOrder storeOrder;

    // 입고일
    @Column(nullable = false)
    private LocalDate dateIn;

}
