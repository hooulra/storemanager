package com.hoonhee92.unstaffedstoremanager.interfaces;

public interface CommonModelBuilder<T> {
        T build();
    }
