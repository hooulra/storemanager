package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StoreInItem {
    private Long id;
    private String productName;
    private Integer orderQuantity;
    private LocalDate dateIn;
}
