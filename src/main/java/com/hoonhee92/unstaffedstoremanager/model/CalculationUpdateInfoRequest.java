package com.hoonhee92.unstaffedstoremanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CalculationUpdateInfoRequest {
    @NotNull
    private Integer quantity;
}
