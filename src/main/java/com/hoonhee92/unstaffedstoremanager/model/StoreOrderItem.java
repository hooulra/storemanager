package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import com.hoonhee92.unstaffedstoremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StoreOrderItem {
    // 상품 id
    private Long productId;

    // 원가
    private Double costPrice;

    // 수량
    private Integer orderQuantity;

    // 발주일
    private LocalDate dateOrder;

    // 수정일
    private LocalDate dateRevision;

    public StoreOrderItem(Builder builder) {
        this.productId = builder.productId;
        this.costPrice = builder.costPrice;
        this.orderQuantity = builder.orderQuantity;
        this.dateOrder = builder.dateOrder;
        this.dateRevision = builder.dateRevision;
    }

    public static class Builder implements CommonModelBuilder<StoreOrderItem> {
        private final Long productId;
        private final Double costPrice;
        private final Integer orderQuantity;
        private final LocalDate dateOrder;
        private final LocalDate dateRevision;

        public Builder(StoreOrder storeOrder) {
            this.productId = storeOrder.getProduct().getId();
            this.costPrice = storeOrder.getCostPrice();
            this.orderQuantity = storeOrder.getOrderQuantity();
            this.dateOrder = storeOrder.getDateOrder();
            this.dateRevision = storeOrder.getDateRevision();
        }

        @Override
        public StoreOrderItem build() {
            return new StoreOrderItem(this);
        }
    }
}

