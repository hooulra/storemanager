package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
@Getter
@Setter
public class StoreOutRequest {
    @NotNull
    private Product product;

    @NotNull
    private Integer outQuantity;

    @NotNull
    private LocalDate datePayment;
}
