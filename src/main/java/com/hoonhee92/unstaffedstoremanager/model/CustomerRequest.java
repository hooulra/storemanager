package com.hoonhee92.unstaffedstoremanager.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @ApiModelProperty(name = "연락처 (11~11)", required = true)
    @NotNull
    private String contact;

    @ApiModelProperty(name = "비밀번호 (4~4)", required = true)
    @NotNull
    private String password;

    @ApiModelProperty(name = "적립금", required = true)
    @NotNull
    private Double point;
}
