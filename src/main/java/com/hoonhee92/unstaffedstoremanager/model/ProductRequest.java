package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.enums.StorageMethod;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductRequest {
    // 상품명
    @NotNull
    private String productName;
    // 단가
    @NotNull
    private Double unitPrice;
    // 할인금액
    @NotNull
    private Double discountPrice;
    // 바코드
    @NotNull
    private String barcode;
    // 재고수량
    @NotNull
    private Integer quantity;
    // 보관방법
    @NotNull
    @Enumerated(EnumType.STRING)
    private StorageMethod storageMethod;
}
