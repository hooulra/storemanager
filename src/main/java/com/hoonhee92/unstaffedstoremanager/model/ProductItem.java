package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.enums.StorageMethod;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class ProductItem {
    private Long id;

    private String productName;

    private Double unitPrice;

    private Double discountPrice;

    private Integer quantity;

    private String storageMethod;
}
