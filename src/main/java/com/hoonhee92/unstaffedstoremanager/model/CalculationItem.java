package com.hoonhee92.unstaffedstoremanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalculationItem {
    private Long id;
    private String productName;
    private Double unitPrice;
    private Integer quantity;
    private Double price;
}
