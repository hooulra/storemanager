package com.hoonhee92.unstaffedstoremanager.model;

import com.hoonhee92.unstaffedstoremanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter

public class StoreOrderUpdateInfoRequest {
    @ApiModelProperty(notes = "원가")
    @NotNull
    @Min(0)
    private Double costPrice;

    @ApiModelProperty(notes = "수량")
    @NotNull
    @Min(0)
    private Integer orderQuantity;

    @NotNull
    private LocalDate dateRevision;
}
