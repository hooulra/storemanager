package com.hoonhee92.unstaffedstoremanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnstaffedStoreManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnstaffedStoreManagerApplication.class, args);
    }

}
