package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import com.hoonhee92.unstaffedstoremanager.model.StoreInItem;
import com.hoonhee92.unstaffedstoremanager.service.StoreInService;
import com.hoonhee92.unstaffedstoremanager.service.StoreOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "입고 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-in")
public class StoreInController {
    private final StoreOrderService storeOrderService;
    private final StoreInService storeInService;

    @ApiOperation(value = "입고 내역 등록")
    @PostMapping("/data/{storeOrderId}")
    public String setStoreIn(@PathVariable long storeOrderId) {
        StoreOrder storeOrder = storeOrderService.getData(storeOrderId);

        storeInService.setStoreIn(storeOrder);

        return "OK";
    }

    @ApiOperation(value = "입고 내역 리스트")
    @GetMapping("/all")
    public List<StoreInItem> getStoreInList() {
        return storeInService.getStoreInList();
    }
}
