package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.service.DisposeRefundService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "폐기 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/dispose")
public class DisposeProductController {
    private final DisposeRefundService barcodeService;

//    @PutMapping("/update")
//    public String putDisposeProduct(String barcode) {
//        barcodeService.putDisposeProduct(barcode);
//
//        return "OK";
//    }
}
