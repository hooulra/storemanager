package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.model.CustomerRequest;
import com.hoonhee92.unstaffedstoremanager.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@Api(tags = "고객 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @ApiOperation(value = "고객 등록")
    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "OK";
    }


}
