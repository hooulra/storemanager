package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.model.CalculationItem;
import com.hoonhee92.unstaffedstoremanager.service.CalculationService;
import com.hoonhee92.unstaffedstoremanager.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "계산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calculation")
public class CalculationController {
    private final CalculationService calculationService;

    @ApiOperation(value = "계산 물품 등록")
    @PostMapping("/data")
    public String setCalculator(String barcode) {
        calculationService.setCalculation(barcode);

        return "OK";
    }

    @ApiOperation(value = "계산 물품 리스트")
    @GetMapping("/all")
    public List<CalculationItem> getCalculations() {
        return calculationService.getCalculations();
    }

    @ApiOperation(value = "수량 증가")
    @PutMapping("/quantity-plus/{id}")
    public String putPlusQuantity(@PathVariable long id) {
        calculationService.putPlusQuantity(id);

        return "OK";
    }

    @ApiOperation(value = "수량 감소")
    @PutMapping("/quantity-minus/{id}")
    public String putMinusQuantity(@PathVariable long id) {
        calculationService.putMinusQuantity(id);

        return "OK";
    }

    @ApiOperation(value = "부분 취소")
    @DeleteMapping("/select-del/{id}")
    public String delSelectCalculation(long id) {
        calculationService.delSelectCalculation(id);

        return "OK";
    }

    @ApiOperation(value = "전체 취소")
    @DeleteMapping("/all-del")
    public String delAllCalculation() {
        calculationService.delAllCalculation();

        return "OK";
    }
}
