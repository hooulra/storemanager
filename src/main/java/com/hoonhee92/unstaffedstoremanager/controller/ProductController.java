package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.model.ProductItem;
import com.hoonhee92.unstaffedstoremanager.model.ProductRequest;
import com.hoonhee92.unstaffedstoremanager.model.ProductUpdateInfoRequest;
import com.hoonhee92.unstaffedstoremanager.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "상품 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product")
public class ProductController {
    private final ProductService productService;

    @ApiOperation(value = "상품 정보 등록")
    @PostMapping("/data")
    public String setProduct(@RequestBody @Valid ProductRequest request) {
        productService.setProduct(request);

        return "OK";
    }

    @ApiOperation(value = "상품 정보 리스트")
    @GetMapping("/all")
    public List<ProductItem> getProducts() {
        return productService.getProducts();
    }

//    @ApiOperation(value = "상품 정보 수정")
//    @PutMapping("/update/{id}")
//    public String putProduct(@PathVariable long id, @RequestBody @Valid ProductUpdateInfoRequest request) {
//        productService.putProduct(id, request);
//
//        return "OK";
//    }

    @ApiOperation(value = "매진 임박 리스트")
    @GetMapping("/sold-soon-all")
    public List<Product> getListSoldOutSoon() {
        return productService.getListSoldOutSoon();
    }
}
