package com.hoonhee92.unstaffedstoremanager.controller;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderItem;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderRequest;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderUpdateInfoRequest;
import com.hoonhee92.unstaffedstoremanager.service.ProductService;
import com.hoonhee92.unstaffedstoremanager.service.StoreOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "발주 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/store-order")
public class StoreOrderController {
    private final StoreOrderService storeOrderService;
    private final ProductService productService;

    @ApiOperation(value = "발주 등록")
    @PostMapping("/data/{productId}")
    public String setStoreOrder(@PathVariable long productId, @RequestBody @Valid StoreOrderRequest request) {
        Product product = productService.getData(productId);

        storeOrderService.setStoreOrder(product, request);

        return "OK";
    }

    @ApiOperation(value = "발주 내역 수정")
    @PutMapping("/update/{id}")
    public String putStoreOrder(@PathVariable long id, @RequestBody @Valid StoreOrderUpdateInfoRequest request) {
        storeOrderService.putStoreOrder(id, request);

        return "OK";
    }

    @ApiOperation(value = "발주 내역 리스트")
    @GetMapping("/all")
    public List<StoreOrderItem> getStoreItems() {
        return storeOrderService.getStoreOrderItems();
    }

}
