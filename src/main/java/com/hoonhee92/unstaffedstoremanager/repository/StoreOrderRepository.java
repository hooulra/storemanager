package com.hoonhee92.unstaffedstoremanager.repository;

import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreOrderRepository extends JpaRepository<StoreOrder, Long> {
}
