package com.hoonhee92.unstaffedstoremanager.repository;

import com.hoonhee92.unstaffedstoremanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
