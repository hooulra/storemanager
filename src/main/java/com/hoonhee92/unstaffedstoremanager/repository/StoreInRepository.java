package com.hoonhee92.unstaffedstoremanager.repository;

import com.hoonhee92.unstaffedstoremanager.entity.StoreIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreInRepository extends JpaRepository<StoreIn, Long> {
}
