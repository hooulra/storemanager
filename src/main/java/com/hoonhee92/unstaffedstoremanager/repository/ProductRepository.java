package com.hoonhee92.unstaffedstoremanager.repository;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByQuantityIsLessThanEqualOrderByQuantityDesc (int count);

    Product findByBarcode (String barcodeNumber);
}
