package com.hoonhee92.unstaffedstoremanager.repository;

import com.hoonhee92.unstaffedstoremanager.entity.StoreOut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreOutRepository extends JpaRepository<StoreOut, Long> {
}
