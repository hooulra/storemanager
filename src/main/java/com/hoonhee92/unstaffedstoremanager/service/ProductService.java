package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.model.ProductItem;
import com.hoonhee92.unstaffedstoremanager.model.ProductRequest;
import com.hoonhee92.unstaffedstoremanager.model.ProductUpdateInfoRequest;
import com.hoonhee92.unstaffedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Product getData(long id) {
        return productRepository.findById(id).orElseThrow();
    }

    // 요구사항 4번 : 상품 정보 등록
    public void setProduct(ProductRequest request) {
        Product addData = new Product.Builder(request).build();

        productRepository.save(addData);
    }

    // 요구사항 6번 : 상품 정보 리스트
    public List<ProductItem> getProducts() {
        List<Product> originData = productRepository.findAll();

        List<ProductItem> result = new LinkedList<>();

        originData.forEach(e -> {
            ProductItem addData = new ProductItem();
            addData.setProductName(e.getProductName());
            addData.setUnitPrice(e.getUnitPrice());
            addData.setDiscountPrice(e.getDiscountPrice());
            addData.setQuantity(e.getQuantity());
            addData.setStorageMethod(e.getStorageMethod().getStorageName());

            result.add(addData);
        });
        return result;

    }

    // 요구사항 5번 : 상품 정보 수정
//    public void putProduct(long id, ProductUpdateInfoRequest request) {
//        Product originData = productRepository.findById(id).orElseThrow();
//
//        originData.setProductName(request.getProductName());
//        originData.setUnitPrice(request.getUnitPrice());
//        originData.setDiscountPrice(request.getDiscountPrice());
//        originData.setQuantity(request.getQuantity());
//        originData.setStorageMethod(request.getStorageMethod());
//
//        productRepository.save(originData);
//    }

    // 요구사항 7번 : 매진 임박 리스트
    // 해결 사항 : 현재 모두 불러왔을때 enum 값이 name 으로 안나오고 원형으로 나옴
    public List<Product> getListSoldOutSoon() {
        return productRepository.findAllByQuantityIsLessThanEqualOrderByQuantityDesc(10);
    }





}
