package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderItem;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderRequest;
import com.hoonhee92.unstaffedstoremanager.model.StoreOrderUpdateInfoRequest;
import com.hoonhee92.unstaffedstoremanager.repository.StoreOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreOrderService {
    private final StoreOrderRepository storeOrderRepository;

    // 요구사항 9번 : 발주 물품 등록
    public void setStoreOrder(Product product, StoreOrderRequest request) {
        storeOrderRepository.save(new StoreOrder.Builder(product,request).build());
    }

    // 요구사항 11번 : 발주 물품 수정
    public void putStoreOrder(long id, StoreOrderUpdateInfoRequest request) {
        StoreOrder originData = storeOrderRepository.findById(id).orElseThrow();

        originData.putStoreOrder(request);

        storeOrderRepository.save(originData);
    }

    // 요구사항 12번 : 발주 내역 리스트
    public List<StoreOrderItem> getStoreOrderItems() {
        List<StoreOrder> originData = storeOrderRepository.findAll();
        List<StoreOrderItem> result = new LinkedList<>();
        originData.forEach(e -> result.add(new StoreOrderItem.Builder(e).build()));
        return result;
    }

    // storeOrder id 값 가져와주는 로직
    public StoreOrder getData(long id) {
        return storeOrderRepository.findById(id).orElseThrow();
    }
}
