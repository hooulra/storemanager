package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DisposeRefundService {
    private final ProductRepository productRepository;

//    // 요구사항 8번 : 상품 폐기 처리
//    public void putDisposeProduct(String barcode) {
//        Product barcodeData = productRepository.findByBarcode(barcode);
//        barcodeData.setQuantity(barcodeData.getQuantity()-1);
//
//        productRepository.save(barcodeData);
//    }
//
//    // 요구사항 9번 : 상품 환불 처리
//
//    public void putRefund (String barcode) {
//        Product productBarcode = productRepository.findByBarcode(barcode);
//        productBarcode.setQuantity(productBarcode.getQuantity()-1);
//
//
//    }
//
//
}
