package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.StoreIn;
import com.hoonhee92.unstaffedstoremanager.entity.StoreOrder;
import com.hoonhee92.unstaffedstoremanager.model.StoreInItem;
import com.hoonhee92.unstaffedstoremanager.repository.StoreInRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreInService {
    private final StoreInRepository storeInRepository;

    // 요구사항 13번 : 입고내역 등록
    public void setStoreIn(StoreOrder storeOrder) {
        StoreIn addData = new StoreIn();
        addData.setStoreOrder(storeOrder);
        addData.setDateIn(LocalDate.now());

        storeInRepository.save(addData);
    }

    // 요구사항 14번 : 입고내역 리스트
    public List<StoreInItem> getStoreInList() {
        List<StoreIn> originData = storeInRepository.findAll();
        List<StoreInItem> result = new LinkedList<>();

        originData.forEach(e -> {
            StoreInItem addData = new StoreInItem();
            addData.setId(e.getId());
            addData.setProductName(e.getStoreOrder().getProduct().getProductName());
            addData.setOrderQuantity(e.getStoreOrder().getOrderQuantity());
            addData.setDateIn(e.getDateIn());

            result.add(addData);
        });
        return result;
    }

}
