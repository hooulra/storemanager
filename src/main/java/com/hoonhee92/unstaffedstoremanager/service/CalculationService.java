package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.Calculation;
import com.hoonhee92.unstaffedstoremanager.entity.Product;
import com.hoonhee92.unstaffedstoremanager.model.CalculationItem;
import com.hoonhee92.unstaffedstoremanager.model.CalculationUpdateInfoRequest;
import com.hoonhee92.unstaffedstoremanager.repository.CalculationRepository;
import com.hoonhee92.unstaffedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CalculationService {
    private final ProductRepository productRepository;
    private final CalculationRepository calculationRepository;


    // 요구사항 15번 : 바코드 정보 조회 1개씩 등록
    public void setCalculation(String barcode) {
        Product barcodeData = productRepository.findByBarcode(barcode);
        Calculation addData = new Calculation();
        addData.setProductName(barcodeData.getProductName());
        addData.setUnitPrice(barcodeData.getUnitPrice());
        addData.setQuantity(1);
        addData.setPrice(barcodeData.getUnitPrice() * addData.getQuantity());

        calculationRepository.save(addData);


    }

    // 요구사항 15번 : 찍은 바코드 리스트
    public List<CalculationItem> getCalculations() {
        List<Calculation> originData = calculationRepository.findAll();
        List<CalculationItem> result = new LinkedList<>();

        originData.forEach(e -> {
            CalculationItem addData = new CalculationItem();
            addData.setId(e.getId());
            addData.setProductName(e.getProductName());
            addData.setUnitPrice(e.getUnitPrice());
            addData.setQuantity(e.getQuantity());
            addData.setPrice(e.getPrice());
            result.add(addData);
        });
        return result;
    }

    // 요구사항 16번 : 수량 증가
    public void putPlusQuantity(long id) {
        Calculation calculation = calculationRepository.findById(id).orElseThrow();

        calculation.setQuantity(calculation.getQuantity() + 1);

        calculationRepository.save(calculation);
    }

    // 요구사항 17번 : 수량 감소
    public void putMinusQuantity(long id) {
        Calculation calculation = calculationRepository.findById(id).orElseThrow();

        calculation.setQuantity(calculation.getQuantity() - 1);

        calculationRepository.save(calculation);
    }

    // 요구사항 18번 : 부분 취소
    public void delSelectCalculation(long id) {
        calculationRepository.deleteById(id);
    }

    // 요구사항 19번 : 전체 취소
    public void delAllCalculation() {
        calculationRepository.deleteAll();
    }

    // 계산 목록 반환
    public List<Calculation> getData() {
        return calculationRepository.findAll();
    }
}
