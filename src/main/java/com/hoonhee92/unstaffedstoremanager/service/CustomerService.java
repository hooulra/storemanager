package com.hoonhee92.unstaffedstoremanager.service;

import com.hoonhee92.unstaffedstoremanager.entity.Customer;
import com.hoonhee92.unstaffedstoremanager.model.CustomerRequest;
import com.hoonhee92.unstaffedstoremanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    // 요구사항 1번 : 고객 등록
    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();

        addData.setContact(request.getContact());
        addData.setPassword(request.getPassword());
        addData.setPoint(0D);

        customerRepository.save(addData);
    }
}
